# Profile Setup

1. Install fish shell 
   - sudo yum install --enablerepo='UCAT_Red_Hat_EPEL_EPEL_Server_7' --assumeyes fish
1. Set user shell to /usr/bin/fish
   - Override user info on host
1. Install neovim
   - sudo yum install --enablerepo='UCAT_Red_Hat_EPEL_EPEL_Server_7' --assumeyes neovim python2-neovim python34-neovim
