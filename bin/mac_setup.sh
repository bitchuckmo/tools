#!/bin/bash
#
# Setup toolboox on a Mac
#
# ENVIRONMENT VARIABLES
#
# defaults
ls -1d defaults.env && source defaults.env
# user over rides
ls -1d ~/.toolbox.env 2>&1 1>/dev/null && source ~/.toolbox.env

echo TB_BREW_INSTALL - ${TB_BREW_INSTALL}

if ${TB_BREW_INSTALL:-False} ;then
    echo checking for brew and ansible
    # install brew if missing
    ls -1d /usr/local/bin/brew 2>&1 1>/dev/null ||
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    # install ansible if missing
    ls -1d /usr/local/bin/ansible 2>&1 1>/dev/null ||
        brew install ansible
fi

/usr/local/bin/ansible-playbook $@ mac_setup.yml
