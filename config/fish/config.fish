# Load pyenv automatically by appending
# the following to ~/.config/fish/config.fish:

if not test -d ~/.local/bin
    mkdir -p ~/.local/bin
end

#set -gx PIP_EXTRA_INDEX_URL https://uxtools.ohsu.edu:6868/simple
