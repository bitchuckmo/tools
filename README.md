# Chuck's zsh/neovim toolbox

## setup toolbox

```bash
cd ~
https://morelanc@ugitlab.ohsu.edu/morelanc/toolbox.git
#TBD#
```

## tools and links used: 

  - [iTerm2 for Mac](https://www.iterm2.com/)
  - [Homebrew - Mac Package Manager](https://brew.sh/)
    - ``brew install python3``
    - ``brew install neovim``
    - ``brew cask install pdftotext``
    - ``brew cask install pdftotext``
    - 
  - [DiffMerge](http://www.sourcegear.com/diffmerge/downloaded.php)

  
### linux (ubuntu/rhel) setup


### mac setup
